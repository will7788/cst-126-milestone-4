<?php
/*CST-126 Milestone 3, William Thornton, Version 1.0, 06/16/2019 */
    
    session_start();
    require('myFuncs.php');
    $con = dbConnect();

    //select first name and last name only if both username AND password match.
    $sql = "SELECT post_id, post_title, post_content, posted_by, deleted_flag, category_id FROM posts";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($post_id, $post_title, $post_content, $posted_by, $deleted_flag, $category_id);
    $rows = $stmt->num_rows;
    if ($rows > 0){
        while($stmt->fetch()){
            if ($deleted_flag == 'n')
            {
                echo '<div class="signup" style="margin-left:5%; width:90%; margin-top:1rem;">';
                echo '<h1 style="text-align:center;">'.$post_title.'</h1>';
                echo "<p>Posted by user with ID: ".$posted_by."</p>";
                echo "<p>".$post_content."</p>";
                echo "<p>Post Category ID: ".$category_id."</p>";
                if ($_SESSION['userrole'] == 1)
                {
                    echo '<form action="removePost.php" method="post" name="input" class="">
                        <input type="hidden" name="removebtn" value="'.$post_id.'">
                        <input class="btn btn-dark btn-lg submission" type="submit" 
                        name="submit" value="Remove" style="margin-left:0;">
                        </form>';
                    echo '<form action="changeCategory.php" method="post" name="input">
                        <select class="selectpicker" name="category">
                             <option value="1">1</option>
                             <option value="2">2</option>
                             <option value="3">3</option>
                         </select>
                        <input type="hidden" name="removebtn" value="'.$post_id.'">
                        <input class="btn btn-dark btn-lg submission" type="submit" 
                        name="submit" value="Change Post Category" style="margin-left:0;">
                        </form>';
                }
                echo '</div>';
            }
        }

    }
    elseif($rows == 0){
        echo "<p>There are no posts</p>";
    }
    else{
        die('Connect Error:' . $con->connect_error);
    }

    $con->close();

?>