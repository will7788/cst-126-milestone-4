<?php
/*CST-126 Milestone 3, William Thornton, Version 1.0, 06/16/2019 */
	require('myFuncs.php');
	$con = dbConnect();

	$username = $_POST[uname];
	$firstname = $_POST[fname];
	$lastname = $_POST[lname];
	$email = $_POST[mail];
	$pword = $_POST[pword];
	$cpword = $_POST[cpword];
	$birthday = $_POST[bday];
	$birthday = $birthday + 0;
	$birthmonth = $_POST[bmonth];
	$birthmonth = $birthmonth + 0;
	$birthyear = $_POST[byear];
	$birthyear = $birthyear + 0;

	if (empty($firstname))
	{
		echo 'The First Name is a required field and cannot be blank.';
		echo '<br>';
	}
	if (empty($lastname))
	{
        echo 'The Last Name is a required field and cannot be blank.';
        echo '<br>';
    }
    if (empty($username))
	{
        echo 'The Username is a required field and cannot be blank.';
        echo '<br>';
	}
	if (empty($email))
	{
		echo 'The Email is a required field and cannot be blank.';
		echo '<br>';
	}
    if (empty($pword))
	{
		echo 'The Password is a required field and cannot be blank.';
	}
	if (empty($firstname) || empty($lastname) || empty($username) || empty($pword) || empty($email))
	{
		exit();
	}

	if ($cpword != $pword)
	{
		echo "<p>Your passwords do not match.</p>";
		exit();
	}
	
	
	$sql = "INSERT INTO user_info 
	(username, firstname, lastname, email, PASS_WORD, birth_day, 
	birth_month, birth_year) VALUES ('$username','$firstname',
	'$lastname', '$email', '$pword', '$birthday', '$birthmonth', '$birthyear' )";
	
	if (!mysqli_query($con,$sql))
	{
		echo 'Not Inserted';
	}
	else 
	{
		echo 'You have been signed up, please wait while you are redirected.';
	}
	
	header("refresh:1; url=login.html")
?>