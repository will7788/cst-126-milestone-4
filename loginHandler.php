<?php
/*CST-126 Milestone 3, William Thornton, Version 1.0, 06/16/2019 */
    
    session_start();
    require('myFuncs.php');
    $con = dbConnect();

    $uname = $_POST[uname];
    $pword = $_POST[pword];

    if (empty($uname))
	{
        echo 'The Username is a required field and cannot be blank.';
        echo '<br>';
    }
    if (empty($pword))
	{
		echo 'The Password is a required field and cannot be blank.';
	}
	if (empty($uname) || empty($pword))
	{
		exit();
    }
    //select first name and last name only if both username AND password match.
    $sql = "SELECT id, firstname, lastname, email, birth_day, birth_month, birth_year, user_role FROM user_info WHERE username=? AND PASS_WORD=?";
    $stmt = $con->prepare($sql);
    $stmt->bind_param('ss', $uname, $pword);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($id, $firstname, $lastname, $email, $birthday, $birthmonth, $birthyear, $userrole);
    $rows = $stmt->num_rows;

    if ($rows == 1){
        while($stmt->fetch()){
            $_SESSION["id"] = $id;
            $_SESSION["fname"] = $firstname;
            $_SESSION["lname"] = $lastname;
            $_SESSION["email"] = $email;
            $_SESSION["birthday"] = $birthday;
            $_SESSION["birthmonth"] = $birthmonth;
            $_SESSION["birthyear"] = $birthyear;
            $_SESSION["userrole"] = $userrole;
        }
        header("Location: profile.php");
    }
    elseif($rows == 0){
        echo "<p>Login Unsuccessful!</p>";
    }
    elseif($rows > 1){
        echo "<p>Multiple users registered with those credentials.</p>";
    }
    else{
        die('Connect Error:' . $con->connect_error);
    }

    $con->close();

?>