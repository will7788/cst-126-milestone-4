<?php
/*CST-126 Milestone 3, William Thornton, Version 1.0, 06/16/2019*/
    session_start();
	require('myFuncs.php');
	$con = dbConnect();

	$ptitle = $_POST[posttitle];
    $pcontent = $_POST[postcontent];
    $id = $_SESSION["id"];

	if (empty($ptitle))
	{
		echo 'The Post Title is a required field and cannot be blank.';
        echo '<br>';
        exit();
	}
	if (empty($pcontent))
	{
        echo 'The Post Content is a required field and cannot be blank.';
        echo '<br>';
        exit();
    }

    if (strlen($ptitle) > 255)
    {
        echo 'The Post Title cannot be more than 255 characters.';
        echo '<br>';
        exit();
    }
    
    if (strlen($pcontent) > 10000)
    {
        echo 'The Post Content cannot be more than 10000 characters.';
        echo '<br>';
        exit();
    }

    $pcontent = contentFilter($pcontent);
    $ptitle = contentFilter($ptitle);
	
	
	$sql = "INSERT INTO posts 
	(post_title, post_content, posted_by, updated_by) VALUES ('$ptitle','$pcontent',
	'$id', '$id')";
	
	if (!mysqli_query($con,$sql))
	{
		echo 'Not Posted!';
	}
	else 
	{
		echo 'Posted!';
	}
	
	header("refresh:1; url=posts.php");
?>